import { IndexComponent } from './index/index.component';
import { Routes } from '@angular/router';
export const routes: Routes = [
    {
        path: '',
        redirectTo: 'index/1',
        pathMatch: 'full'
    },
    {
        path: 'index/:pid',
        component: IndexComponent,
        data: { title: 'my blog'}
    },
    {
        path: '**',
        redirectTo: 'index/1',
        pathMatch: 'full'
    }
];