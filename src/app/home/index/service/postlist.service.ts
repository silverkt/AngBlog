import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class PostlistService {
  constructor(public httpClient: HttpClient) { }

  getPostLists() {
    return this.httpClient.get('http://ceos.webcity3d.com/images/bg/list1.json')
  }

}
