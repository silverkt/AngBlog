import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';

import { PostlistService } from './service/postlist.service';


 

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css'],
  providers: [ PostlistService ]
})
export class IndexComponent implements OnInit {

  lists: Array<any>; 
  constructor(public activatedRoute: ActivatedRoute, public title: Title, public postlist: PostlistService) { }
  ngOnInit() {

    this.activatedRoute.params.subscribe((params) => {
      this.getCurrentList(params.pid);
    });
    this.activatedRoute.data.subscribe((data) => {
      this.title.setTitle(data.title);
    });
  }

  public getCurrentList(pid) {
    this.postlist.getPostLists().subscribe((rs: any) => {
      rs.lists.reverse();
      this.lists = rs.lists;
      console.log('http request postlist pageid'+ pid);
    });
  }

}
