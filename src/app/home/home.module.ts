import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http'
import { routes } from './home.route';

import { SharedModule } from '../shared/shared.module';

import { IndexComponent } from './index/index.component';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    RouterModule.forChild(routes),
    SharedModule
  ],
  declarations: [IndexComponent]
})
export class HomeModule { }
