import { Component, OnInit } from '@angular/core';
import { CategoryService } from './service/category.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css'],
  providers: [ CategoryService ]
})
export class CategoryComponent implements OnInit {
  public lists: any;
  constructor(public activatedRoute:ActivatedRoute, public categoryService: CategoryService) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe( param => {
      this.categoryService.getCategoryList(param.cid).subscribe((rs: any) => {
        rs.lists.reverse();
        this.lists = rs.lists;
        console.log(rs.lists);
      });
    })
    
  }

}
