import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class CategoryService {

  constructor(public httpClient: HttpClient) { }

  getCategoryList(cid: number) {
    return this.httpClient.get('http://ceos.webcity3d.com/images/bg/catelist1.json');
  }
}
