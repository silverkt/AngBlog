import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { Location } from '@angular/common';

import { ArticleService } from './service/article.service';



@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.css'],
  providers: [ ArticleService ]
})
export class ArticleComponent implements OnInit {
  public currentArticle: any;
  constructor(public activatedRoute: ActivatedRoute, public title: Title, public article: ArticleService, public location: Location) { }

  ngOnInit() {
    this.activatedRoute.data.subscribe((data) => {
      this.title.setTitle(data.title);
    });
    this.activatedRoute.params.subscribe(rs => {
    	console.log(rs.aid);
      this.article.getArticleContent(rs.aid).subscribe( (rs: any) => {
        this.currentArticle = rs;
        console.log(this.currentArticle);
      })
    });
  }


  public goBack() {
    this.location.back();
  }

}
