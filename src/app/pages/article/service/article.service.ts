import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class ArticleService {

  constructor(public httpClient: HttpClient) { }

  public getArticleContent(aid: string) {
  	return this.httpClient.get('http://ceos.webcity3d.com/images/bg/article1.json');
  }

}
