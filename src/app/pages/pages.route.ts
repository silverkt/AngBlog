import { Routes } from '@angular/router';
import { ArticleComponent } from './article/article.component';
import { AboutComponent } from './about/about.component';
import { CategoryComponent } from './category/category.component';
import { TimelineComponent } from './timeline/timeline.component';
import { LeavemsgComponent } from './leavemsg/leavemsg.component';
export const routes: Routes = [
    {
        path: '',
        redirectTo: 'article/:aid',
        pathMatch: 'full'
    },
    {
        path: 'article/:aid',
        component: ArticleComponent,
        data: { title: '文章详情'}
    },
    {
        path: 'category/:cid',
        component: CategoryComponent,
        data: { title: '文章分类'}
    },
    {
        path: 'timeline',
        component: TimelineComponent,
        data: { title: '归档'}
    },
    {
        path: 'about',
        component: AboutComponent,
        data: { title: '关于我'}
    },
    {
        path: 'leavemsg',
        component: LeavemsgComponent,
        data: { title: '留言'}
    },
    {
        path: '**',
        redirectTo: 'article/1',
        pathMatch: 'full'
    }
];