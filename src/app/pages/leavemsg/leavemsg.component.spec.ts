import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeavemsgComponent } from './leavemsg.component';

describe('LeavemsgComponent', () => {
  let component: LeavemsgComponent;
  let fixture: ComponentFixture<LeavemsgComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeavemsgComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeavemsgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
