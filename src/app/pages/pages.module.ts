import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { SharedModule } from '../shared/shared.module';
import { routes } from './pages.route';

import { ArticleComponent } from './article/article.component';
import { AboutComponent } from './about/about.component';
import { CategoryComponent } from './category/category.component';
import { TimelineComponent } from './timeline/timeline.component';
import { LeavemsgComponent } from './leavemsg/leavemsg.component';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    SharedModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ArticleComponent, AboutComponent, CategoryComponent, TimelineComponent, LeavemsgComponent]
})
export class PagesModule { }
