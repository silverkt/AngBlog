import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PagerService } from './service/pager.service';
import { Observable} from 'rxjs/Rx';
// import 'rxjs/add/operator/switchMap';
// import 'rxjs/add/operator/map';
// import 'rxjs/add/operator/merge';

@Component({
  selector: 'app-pager',
  templateUrl: './pager.component.html',
  styleUrls: ['./pager.component.css'],
  providers: [ PagerService ]
})
export class PagerComponent implements OnInit {
   public total: number;
   public pageArray: number[];	
   public current: number;
  @Input() 
  location: string;	
  constructor( public pager: PagerService, public activatedRoute: ActivatedRoute, public router: Router) { }

  ngOnInit() {

    this.activatedRoute.params.subscribe( params => {
        this.current = params.pid;
        this.pager.getPagesList().subscribe( (rs: any) => {
          let start = 1;
          let end = rs.total;
          if(this.current - 5 > 0) {
            if(+this.current + 5 < +rs.total) {
              start = this.current - 5;
              end = +this.current + 5;
            } else {
              start = rs.total - 10;
              end = rs.total;
            }
          } else {
            start = 1;
            end = (10 < +rs.total) ? 10 : +rs.total;
          }
          
          this.pageArray = this.generatePageArray(start, end);
          this.total = +rs.total;
          //console.log(this.total);
        });
    });   
  }

  public prePage() {
    if (this.current > 1) {
      this.router.navigate(['/home/index', +this.current-1]);
    }    
  }

  public nextPage() {
    if (this.current < this.pageArray[this.pageArray.length -1 ]) {
      this.router.navigate(['/home/index', +this.current+1]);
    }
  }

  public generatePageArray(start: number, end: number): Array<number> {
    let pager = [];
    for (let i=start; i<=end; i++) {
      pager.push(i);
    }
    return pager;
  }

}
