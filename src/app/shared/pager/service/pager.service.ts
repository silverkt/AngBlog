import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class PagerService {

  constructor(public httpClient: HttpClient) { }
  getPagesList() {
  	return this.httpClient.get('http://ceos.webcity3d.com/images/bg/pager.json');
  }

}
