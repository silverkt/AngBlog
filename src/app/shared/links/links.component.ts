import { Component, OnInit } from '@angular/core';
import { LinksService } from './service/links.service';

@Component({
  selector: 'app-links',
  templateUrl: './links.component.html',
  styleUrls: ['./links.component.css'],
  providers: [ LinksService ]
})
export class LinksComponent implements OnInit {

  constructor(public links: LinksService) { }
  public friendLinks: any;
  ngOnInit() {
    this.links.getFriendLinks().subscribe( rs => {
      this.friendLinks = rs;
    });
  }

}
