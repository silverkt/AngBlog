import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class LinksService {

  constructor(public httpClient: HttpClient) { }

  getFriendLinks() {
    return this.httpClient.get('http://ceos.webcity3d.com/images/bg/links.json');
  }

}
