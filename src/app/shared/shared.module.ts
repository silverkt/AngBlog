import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http'
import { CatergComponent } from './caterg/caterg.component';
import { LinksComponent } from './links/links.component';
import { PagerComponent } from './pager/pager.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    HttpClientModule
  ],
  declarations: [CatergComponent, LinksComponent, PagerComponent],
  exports: [CatergComponent, LinksComponent, PagerComponent]
})
export class SharedModule { }
