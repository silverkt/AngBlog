import { TestBed, inject } from '@angular/core/testing';

import { CatergService } from './caterg.service';

describe('CatergService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CatergService]
    });
  });

  it('should be created', inject([CatergService], (service: CatergService) => {
    expect(service).toBeTruthy();
  }));
});
