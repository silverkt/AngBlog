import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class CatergService {

  constructor(public httpClient: HttpClient) { }
  getCatergories() {
  	return this.httpClient.get('http://ceos.webcity3d.com/images/bg/catergory.json');
  }
}
