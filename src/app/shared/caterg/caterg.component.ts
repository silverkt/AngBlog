import { Component, OnInit } from '@angular/core';
import { RouterLink } from '@angular/router';
import { CatergService  } from './service/caterg.service';

@Component({
  selector: 'app-caterg',
  templateUrl: './caterg.component.html',
  styleUrls: ['./caterg.component.css'],
  providers: [ CatergService  ]
})
export class CatergComponent implements OnInit {
    public myCatergory: any;	
  constructor( public catergory: CatergService) { }

  ngOnInit() {
  	this.catergory.getCatergories().subscribe( rs => {
  		this.myCatergory = rs;
  	});
  }

}
