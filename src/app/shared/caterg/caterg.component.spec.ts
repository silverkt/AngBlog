import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CatergComponent } from './caterg.component';

describe('CatergComponent', () => {
  let component: CatergComponent;
  let fixture: ComponentFixture<CatergComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CatergComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CatergComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
