import { Component, OnInit } from '@angular/core';
import { CatergService } from '../shared/caterg/service/caterg.service';
declare var $: any;

@Component({
  selector: 'app-navi',
  templateUrl: './navi.component.html',
  styleUrls: ['./navi.component.css'],
  providers: [CatergService]
})
export class NaviComponent implements OnInit {
  public myCatergory: any;	
  constructor(public cateService: CatergService) { }

  ngOnInit() {
    //jquery functions
    $('#jq-handle').hover(function() {
      $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
    }, function() {
      $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
    });

    $('#jq-handle').click(function() {
      setTimeout(function(){
        $('#jq-handle').removeClass("open");
      },100);
     
      $(this).find('.dropdown-menu').stop(true, true).delay(100).fadeOut(300);
    });
    $('#jq-handle.dropdown-menu').click(function(){
      $('#jq-handle').addClass("open");
    });
 
    //jquery functions end

    this.cateService.getCatergories().subscribe( rs => {
      this.myCatergory = rs;
    })
  }

}
