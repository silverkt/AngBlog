# AngBlog

A fat front-end , sin back-end blog system.

The reason to build this system is : I often use markdown to write documents on local machine , the md file can easily be turn into pdf format or other format .  So I think , I can write the md file once , then use the scripts to turn into different format , like json , then the blog system can also use this result.

## Front-end

Using AngularJs to build this blog system

## Back-end

Using scripts to turn markdown text into json data.  so any server software like nginx , apache  can provide  the static json file for the front.
